package nl.detesters.lib;

import com.google.common.base.Function;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by schenks on 17-4-2015.
 */
public class ExtendedWait {

    private static final Logger LOG = LoggerFactory.getLogger(ExtendedWait.class);

    private static final String JS_JQUERY_DEFINED = "return typeof jQuery != 'undefined';";
    private static final String JS_RICHFACES_DEFINED = "return typeof RichFaces != 'undefined';";
    private static final String JS_JQUERY_ACTIVE = "return jQuery.active != 0;";
    private static final String JS_RICHFACES_QUEUE_NOT_EMPTY = "return !RichFaces.queue.isEmpty();";

    private static final int TIME_OUT_SECONDS = 30;
    private static final int POLLING_MILLISECONDS = 100;

    /**
     * Method used to - sort of - synchronize ajax calls
     * It will look at the jQuery active queue and the RichFaces queue
     * to determine if an ajax call is in progress.
     * The method will wait until both queue are empty / 0 before continuing.
     *
     * @param webDriver current WebDriver
     */
    public static void waitUntilAjaxCallComplete(final WebDriver webDriver) {
        /*
         * source found for PrimeFaces: http://stackoverflow.com/questions/23300126/selenium-wait-for-primefaces-4-0-ajax-to-process
         * adapted to RichFaces. It's not flawless, but seems to work for some events (like when Postcode is filled).
         */
        try {
            new FluentWait(webDriver).withTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS)
                    .pollingEvery(POLLING_MILLISECONDS, TimeUnit.MILLISECONDS)
                    .until(new Function<WebDriver, Boolean>() {
                               @Override
                               public Boolean apply(WebDriver input) {
                                   boolean ajax = false;
                                   boolean jQueryDefined = executeBooleanJavascript(input, JS_JQUERY_DEFINED);
                                   boolean primeFacesDefined = executeBooleanJavascript(input, JS_RICHFACES_DEFINED);

                                   if (jQueryDefined) {
                                       // jQuery is still active
                                       ajax |= executeBooleanJavascript(input, JS_JQUERY_ACTIVE);
                                   }
                                   if (primeFacesDefined) {
                                       // PrimeFaces queue isn't empty
                                       ajax |= executeBooleanJavascript(input, JS_RICHFACES_QUEUE_NOT_EMPTY);
                                   }

                                   // continue if all ajax request are processed
                                   return !ajax;
                               }
                           }

                    );
        } catch (Exception e0) {
            LOG.error("Fout bij het bepalen van Ajax calls (geen Ajax call gevonden)", e0);
        }
    }

    private static boolean executeBooleanJavascript(WebDriver input, String javascript) {
        return (Boolean) ((JavascriptExecutor) input).executeScript(javascript);
    }
}
