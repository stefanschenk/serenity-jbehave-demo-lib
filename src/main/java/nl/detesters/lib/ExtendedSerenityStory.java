package nl.detesters.lib;

import net.serenitybdd.jbehave.SerenityStory;
import nl.detesters.lib.utils.ConverterUtils;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.steps.ParameterConverters;

/**
 *
 */
public class ExtendedSerenityStory extends SerenityStory {
    public ExtendedSerenityStory() {
        super();
    }

    @Override
    public Configuration configuration() {
        return super.configuration()
                .useParameterConverters(
                      new ParameterConverters()
                        .addConverters(ConverterUtils.customConverters())
                );
    }
}
