package nl.detesters.lib;

import net.serenitybdd.core.pages.RenderedPageObjectView;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.locators.SmartAjaxElementLocator;
import net.thucydides.core.annotations.locators.SmartElementHandler;
import net.thucydides.core.annotations.locators.SmartListHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * Created by schenks on 4-5-2015.
 */
public class RelocateElement {
    private static final Logger LOG = LoggerFactory.getLogger(RelocateElement.class);

    /**
     * Method to find an element in de rendered view of a page object which was stale .
     * Uses reflection to find the used locator.
     * Return the found element or null if no element is found.
     *
     * @param renderedPageObjectView The view of the Rendered PageObject you want to find the element in
     * @param webElementFacade       The element you want to find
     * @return The found element from the Rendered PageObject view
     */
    public static WebElementFacade find(RenderedPageObjectView renderedPageObjectView, WebElementFacade webElementFacade) {
        LOG.debug("Called RelocateElement because of Stale Element Reference Exception.");

        WebElementFacade relocatedElement = null;

        try {
            if (webElementFacade instanceof Proxy) {
                if (java.lang.reflect.Proxy.getInvocationHandler(webElementFacade) instanceof SmartElementHandler) {
                    SmartElementHandler smartElementHandler = (SmartElementHandler) java.lang.reflect.Proxy.getInvocationHandler(webElementFacade);

                    Field fieldLocator = smartElementHandler.getClass().getSuperclass().getDeclaredField("locator");
                    fieldLocator.setAccessible(true);
                    SmartAjaxElementLocator smartAjaxElementLocator = (SmartAjaxElementLocator) fieldLocator.get(smartElementHandler);

                    Field fieldBy = smartAjaxElementLocator.getClass().getSuperclass().getDeclaredField("by");
                    fieldBy.setAccessible(true);

                    if (fieldBy.get(smartAjaxElementLocator) instanceof By) {
                        By by = (By) fieldBy.get(smartAjaxElementLocator);

                        LOG.info("Element ({}) gevonden in RenderedView: {}", by, renderedPageObjectView.elementIsPresent(by));
                        relocatedElement = renderedPageObjectView.find(by);
                    } else {
                        LOG.warn("Onbekende selector gevonden: {}", fieldBy.get(smartAjaxElementLocator));
                    }
                }
            } else {
                Field fieldWebElement = webElementFacade.getClass().getDeclaredField("webElement");
                fieldWebElement.setAccessible(true);
                WebElement webElement = (WebElement) fieldWebElement.get(webElementFacade);

                Field webElementParent = webElement.getClass().getDeclaredField("parent");
                webElementParent.setAccessible(true);

                Field webElementFoundBy = webElement.getClass().getDeclaredField("foundBy");
                webElementFoundBy.setAccessible(true);
                String foundByIncludingParent = (String) webElementFoundBy.get(webElement);

                String locatorWithMethod = foundByIncludingParent.replace(String.format("[%s] -> ", webElementParent.get(webElement).toString()), "");

                if (locatorWithMethod.startsWith("xpath")) {
                    String locator = locatorWithMethod.replace("xpath:", "").trim();

                    LOG.info("Element ({}) gevonden in RenderedView: {}", locator, renderedPageObjectView.elementIsPresent(By.xpath(locator)));
                    relocatedElement = renderedPageObjectView.find(By.xpath(locator));
                }
            }
        } catch (Exception exceptionInRelocate) {
            LOG.error("Fout bij het zoeken van het element {}", webElementFacade, exceptionInRelocate);
        }

        return relocatedElement;
    }

    /**
     * Method to find a list of elements in de rendered view of a page object which was stale .
     * Uses reflection to find the used locator.
     * Return the found elements or null if no element is found.
     *
     * @param renderedPageObjectView The view of the Rendered PageObject you want to find the element in
     * @param webElementFacades      The list of elements you want to find
     * @return The list of found elements from the Rendered PageObject view
     */
    public static List<WebElementFacade> findAll(RenderedPageObjectView renderedPageObjectView, List<WebElementFacade> webElementFacades) {
        LOG.debug("Called RelocateElement because of Stale Element Reference Exception.");

        List<WebElementFacade> relocatedElements = null;

        try {
            if (java.lang.reflect.Proxy.getInvocationHandler(webElementFacades) instanceof SmartListHandler) {
                SmartListHandler smartListHandler = (SmartListHandler) java.lang.reflect.Proxy.getInvocationHandler(webElementFacades);

                Field fieldLocator = smartListHandler.getClass().getDeclaredField("locator");
                fieldLocator.setAccessible(true);
                SmartAjaxElementLocator smartAjaxElementLocator = (SmartAjaxElementLocator) fieldLocator.get(smartListHandler);

                Field fieldBy = smartAjaxElementLocator.getClass().getSuperclass().getDeclaredField("by");
                fieldBy.setAccessible(true);

                if (fieldBy.get(smartAjaxElementLocator) instanceof By.ById) {
                    By.ById byId = (By.ById) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byId, renderedPageObjectView.elementIsPresent(byId));
                    relocatedElements = renderedPageObjectView.findAll(byId);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByLinkText) {
                    By.ByLinkText byLinkText = (By.ByLinkText) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byLinkText, renderedPageObjectView.elementIsPresent(byLinkText));
                    relocatedElements = renderedPageObjectView.findAll(byLinkText);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByXPath) {
                    By.ByXPath byXPath = (By.ByXPath) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byXPath, renderedPageObjectView.elementIsPresent(byXPath));
                    relocatedElements = renderedPageObjectView.findAll(byXPath);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByCssSelector) {
                    By.ByCssSelector byCssSelector = (By.ByCssSelector) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byCssSelector, renderedPageObjectView.elementIsPresent(byCssSelector));
                    relocatedElements = renderedPageObjectView.findAll(byCssSelector);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByClassName) {
                    By.ByClassName byClassName = (By.ByClassName) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byClassName, renderedPageObjectView.elementIsPresent(byClassName));
                    relocatedElements = renderedPageObjectView.findAll(byClassName);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByName) {
                    By.ByName byName = (By.ByName) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byName, renderedPageObjectView.elementIsPresent(byName));
                    relocatedElements = renderedPageObjectView.findAll(byName);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByPartialLinkText) {
                    By.ByPartialLinkText byPartialLinkText = (By.ByPartialLinkText) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byPartialLinkText, renderedPageObjectView.elementIsPresent(byPartialLinkText));
                    relocatedElements = renderedPageObjectView.findAll(byPartialLinkText);
                } else if (fieldBy.get(smartAjaxElementLocator) instanceof By.ByTagName) {
                    By.ByTagName byTagName = (By.ByTagName) fieldBy.get(smartAjaxElementLocator);

                    LOG.info("Element ({}) gevonden in RenderedView: {}", byTagName, renderedPageObjectView.elementIsPresent(byTagName));
                    relocatedElements = renderedPageObjectView.findAll(byTagName);
                } else {
                    LOG.warn("Onbekende selector gevonden: {}", fieldBy.get(smartAjaxElementLocator));
                }
            }
        } catch (Exception exceptionInRelocate) {
            LOG.error("Fout bij het zoeken van het element {}", webElementFacades, exceptionInRelocate);
        }

        return relocatedElements;
    }

    public static void findAndClick(RenderedPageObjectView renderedPageObjectView, WebElementFacade webElementFacade) {

    }

    private void performActionOnFoundElement(WebElementFacade webElementFacade, String action) {

    }


}
