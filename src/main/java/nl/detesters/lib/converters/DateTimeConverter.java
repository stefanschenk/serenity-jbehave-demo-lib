package nl.detesters.lib.converters;

import org.jbehave.core.steps.ParameterConverters;
import org.joda.time.DateTime;

import java.lang.reflect.Type;

/**
 *
 */
public class DateTimeConverter implements ParameterConverters.ParameterConverter {
    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return DateTime.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public Object convertValue(String value, Type type) {
        return new DateTime(value);
    }
}
